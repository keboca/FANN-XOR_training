# FANN (Fast Artificial Neural Network) - XOR training

A faster way to locally run XOR trainig by implementing FANN.
source: https://www.php.net/manual/en/fann.examples-1.php

## Lando version

Firstly clone this repository:
`git clone git@github.com:kenneth-bolivar-castro/FANN-XOR_training.git fann`

Finally start lando:
`cd fann && lando start`

Lando documentation: https://docs.lando.dev/config/lamp.html
