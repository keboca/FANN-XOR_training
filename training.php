<?php

$num_input = 2;
$num_output = 1;
$num_layers = 3;
$num_neurons_hidden = 3;
$desired_error = 0.001;
$max_epochs = 500000;
$epochs_between_reports = 1000;

if ($ann = fann_create_standard($num_layers, $num_input, $num_neurons_hidden, $num_output)) {

	fann_set_activation_function_hidden($ann, FANN_SIGMOID_SYMMETRIC);
	fann_set_activation_function_output($ann, FANN_SIGMOID_SYMMETRIC);

	$filename = dirname(__FILE__) . "/xor.data";
	if (fann_train_on_file($ann, $filename, $max_epochs, $epochs_between_reports, $desired_error))
		print('xor trained.<br>' . PHP_EOL);

	if (fann_save($ann, dirname(__FILE__) . "/xor_float.net"))
		print('xor_float.net saved.<br><a href="index.php?demo=1">Demo</a>' . PHP_EOL);

	fann_destroy($ann);
}
