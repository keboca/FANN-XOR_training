<?php

if(!isset($_GET['demo'])) {
	echo "<marquee><a href='index.php?demo=1'>FANN (Fast Artificial Neural Network)</a></marquee>";
	phpinfo();die;
}

$train_file = (dirname(__FILE__) . "/xor_float.net");
if (!is_file($train_file))
    die("The file xor_float.net has not been created! Click <a href='training.php'>here</a> to generate it");

$ann = fann_create_from_file($train_file);
if (!$ann)
    die("ANN could not be created");

$input = array(-1, 1);
$calc_out = fann_run($ann, $input);

printf("xor test (%f,%f) -> %f\n", $input[0], $input[1], $calc_out[0]);
fann_destroy($ann);
